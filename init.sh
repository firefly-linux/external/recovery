#!/bin/bash

apt update

apt -y install gcc make

apt -y install libdrm-dev libcurl4-openssl-dev libssl-dev libbz2-dev git
